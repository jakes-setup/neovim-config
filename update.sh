#!/bin/bash

set -e

echo "Updating Neovim config..."

pushd "$(dirname "$0")" >/dev/null

git submodule init
git submodule update
git pull --recurse-submodules

mkdir -p "deps"

# Install nodejs
NODEJSVERSION="v20.12.2"
NODEJSTYPE="linux-x64"
pushd "./deps" >/dev/null
	if [ -x "./bin/node" ] && [ "x$(./bin/node --version)y" = "x${NODEJSVERSION}y" ]; then
		echo "Nodejs is already ${NODEJSVERSION}"
	else
		echo "Installing Nodejs ${NODEJSVERSION}"
		wget -O "node.tar.xz" "https://nodejs.org/dist/${NODEJSVERSION}/node-${NODEJSVERSION}-linux-x64.tar.xz"
		tar xf "node.tar.xz"
		rm -rf "node.tar.xz"
		cp -rf --update ./node-${NODEJSVERSION}-${NODEJSTYPE}/* .
		rm -rf ./node-${NODEJSVERSION}-${NODEJSTYPE}/
	fi
popd >/dev/null

# Install tree-sitter
TREESITTERVERSION="0.22.5"
TREESITTERTYPE="linux-x64"
mkdir -p "./deps/bin"
pushd "./deps/bin" >/dev/null
	if [ -x "./tree-sitter" ] && [ "x$(./tree-sitter --version | cut -d ' ' -f 2)y" = "x${TREESITTERVERSION}y" ]; then
		echo "Tree-sitter is already v${TREESITTERVERSION}"
	else
		echo "Installing Tree-sitter v${TREESITTERVERSION}"
		wget -O "tree-sitter.gz" "https://github.com/tree-sitter/tree-sitter/releases/download/v${TREESITTERVERSION}/tree-sitter-${TREESITTERTYPE}.gz"
		gunzip --force "tree-sitter.gz"
		chmod +x tree-sitter
	fi
popd >/dev/null


# Download and build latest stable neovim release
if [ ! -d "neovim" ]; then
	git clone https://github.com/neovim/neovim.git neovim
fi
pushd neovim

	OLDNEOVIMCOMMIT=""
	if [ -f ../neovimcommit ]; then
		OLDNEOVIMCOMMIT="$(cat ../neovimcommit)"
	fi
	#NEOVIMCOMMIT="$(git rev-parse HEAD)"
	NEOVIMCOMMIT="$(curl 'https://github.com/neovim/neovim/latest-commit/stable'  -H 'Accept: application/json'  | jq -j '.oid')"
	if [ "x${OLDNEOVIMCOMMIT}y" != "x${NEOVIMCOMMIT}y" ]; then
		# Update neovim repo
		git checkout master
		git pull
		git fetch --all --force
		git fetch --tags --force
		git pull
		git checkout stable
		# Rebuild neovim
		rm -rf build/  # clear the CMake cache
		rm -rf .deps/  # fix problems with mismatching build tool versions
		make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$(dirname "$0")/neovim"
		make install

		if hostname | grep -q '^c' ; then
			# Shrink the .git folder on CIP Computers where storage expensive.
			git repack -a -d -f --depth=250 --window=250
		fi
	else
		echo ""
		echo ""
		echo "Neovim is already up-to-date. No need to rebuild."
	fi


	echo ""
	echo ""
	echo ""
	echo "Don't forget to add"
	echo "export PATH=\"\${HOME}/stuff/jakes-setup/neovim-config/neovim/bin:\$PATH\""
	echo "to your .profile file."
	echo ""
	echo ""
	echo ""

	echo "${NEOVIMCOMMIT}" > ../neovimcommit
popd

function install_symlink() {
	SOURCE="$1"
	TARGET="$2"

	if [ ! -L "${TARGET}" ] && [ -e "${TARGET}" ]; then
		echo "${TARGET} is not a symlink. Creating backup..."
		mv -vf "${TARGET}" "${TARGET}-backup-$(date +%Y%m%d-%H%M%S)"
	fi

	rm -rf "${TARGET}"

	SOURCE="$(realpath -e -L "${SOURCE}")"
	TARGET="$(realpath  -L "${TARGET}")"

	ln -v -s -f  "${SOURCE}" "${TARGET}"
}

install_symlink "." "${HOME}/.config/nvim"

# Sync Packagemanager
echo "Updating Lazy plugins"
# This will often crash with a segmentation fault. In that case we execute the command again.
nvim --headless "+Lazy! sync" +qa || nvim --headless "+Lazy! sync" +qa
echo ""

# Update treesitter parsers
echo "Updating Treesitter parsers"
nvim --headless "+TSUpdateSync" +qa
echo ""

# Sync language servers and linters
echo "Updating Mason"
nvim --headless "+MasonUpdate" +qa
echo ""
echo "Updating Mason packages"
nvim --headless "+MasonToolsUpdateSync" +qa
echo ""



popd >/dev/null

echo "Updated Neovim config."
