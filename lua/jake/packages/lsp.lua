-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. Available keys are:
--  - cmd (table): Override the default command used to start the server
--  - filetypes (table): Override the default list of associated filetypes for the server
--  - capabilities (table): Override fields in capabilities. Can be used to disable certain LSP features.
--  - settings (table): Override the default settings passed when initializing the server.
--        For example, to see the options for `lua_ls`, you could go to: https://luals.github.io/wiki/settings/
return {
	clangd = {},
	gopls = {},
	templ = {},
	pyright = {},
	-- rust_analyzer = {},
	tailwindcss = {},
	ts_ls = {
		-- overwrite_server_name = "ts_ls", -- https://github.com/nvim-lua/kickstart.nvim/issues/1128
		cmd = { "node", vim.fn.stdpath("data") .. "/mason/bin/typescript-language-server", "--stdio" },
	},
	eslint = {},
	html = { filetypes = { 'html', 'twig', 'hbs' } },

	lua_ls = {
		-- cmd = {...},
		-- filetypes = { ...},
		-- capabilities = {},
		settings = {
			Lua = {
				completion = {
					callSnippet = 'Replace',
				},
				diagnostics = {
					globals = { 'vim' },
					-- You can toggle below to ignore Lua_LS's noisy `missing-fields` warnings
					--disable = { 'missing-fields' },
				},
				telemetry = { enable = false },
				runtime = {
					-- Tell the language server which version of Lua you're using
					-- (most likely LuaJIT in the case of Neovim)
					version = 'LuaJIT'
				},
				-- Make the server aware of Neovim runtime files
				workspace = {
					checkThirdParty = false,
					library = {
						vim.env.VIMRUNTIME
						-- Depending on the usage, you might want to add additional paths here.
						-- "${3rd}/luv/library"
						-- "${3rd}/busted/library",
					}
					-- or pull in all of 'runtimepath'. NOTE: this is a lot slower
					-- library = vim.api.nvim_get_runtime_file("", true)
				}
			},
		},
	},
	--hls = {},

	jdtls = {
		-- configured via nvim-jdtls. Mason is only used for downloading the language server.
	},
	gradle_ls = {},



	ansiblels = {},
	bashls = {
		--cmd = "node" .. " " .. vim.fn.stdpath("data") .. "/mason/bin/bash-language-server start"
		cmd = { "node", vim.fn.stdpath("data") .. "/mason/bin/bash-language-server", "start" }
	},
	cssls = {},
	-- jqls = {},
	jsonls = {},
	ltex = {
		filetypes = {
			'context',
			'tex',
			'texmf',
			'markdown',
			'rst',
			-- 'html',
			'r',
			'xhtml',
		},
	},
	texlab = {},
	sqlls = {},
	stylelint_lsp = {
		cmd = { "node", vim.fn.stdpath("data") .. "/mason/bin/stylelint-lsp", "--stdio" }
	},
	tinymist = {
		-- Typst LSP https://github.com/Myriad-Dreamin/tinymist
		offset_encoding = "utf-8",
		settings = {
			tinymist = {
				formatterMode = 'typstyle', -- https://github.com/Enter-tainer/typstyle/?tab=readme-ov-file#use-in-your-editor
				exportPdf = "never",
			}
		},

	},

}
