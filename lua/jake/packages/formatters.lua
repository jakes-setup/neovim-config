return {
	--'golines',
	--'gofumpt',
	--'shfmt',
	--'gomodifytags',
	--'gotests',
	'stylua',
	'prettierd',
	-- 'djlint', -- already in linters.lua
	'autoflake',
	'autopep8',
	'clang-format',
}
