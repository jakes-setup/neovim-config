-- Set leader to space
vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.keymap.set("n", "<leader>m", function()
	vim.api.nvim_command('write') -- write current buffer
	--vim.api.nvim_command('term')
	--vim.api.nvim_input('i make clean && make || make\n')
	--vim.api.nvim_command('tabnew term://make')
	vim.api.nvim_command('tabnew term://makehelper.sh')
	vim.api.nvim_command('$') -- go to end of make output
	--vim.api.nvim_input('i')
end
)

vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)


-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })


-- Diagnostic keymaps
vim.keymap.set('n', '<leader>p', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', '<leader>n', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })

-- Fun
vim.keymap.set('n', '<leader>dw', function() vim.cmd.Dealwithit() end, { desc = 'Deal [w]ith it' })
vim.keymap.set('n', '<leader>dp', function() vim.cmd.CellularAutomatonRandom() end, { desc = '[P]lay a random cellular automaton.' })
vim.keymap.set('n', '<leader>dd', function() vim.cmd.DuckHatchRandom(0) end, { desc = 'Hatch a new [d]uck' })
vim.keymap.set('n', '<leader>db', function() vim.cmd.DuckHatchRandom(1) end, { desc = 'Hatch a new [b]ug' })
vim.keymap.set('n', '<leader>dr', function() vim.cmd.DuckHatchRandom() end, { desc = 'Hatch a new [r]andom' })
vim.keymap.set('n', '<leader>dk', function() vim.cmd.DuckCookRandom() end, { desc = 'Coo[k] a random duck' })
vim.keymap.set('n', '<leader>da', function() vim.cmd.DuckCookAll() end, { desc = 'Cook [a]ll ducks' })

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
	callback = function()
		vim.highlight.on_yank()
	end,
	group = highlight_group,
	pattern = '*',
})

-- Remove trailing spaces on write.
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
	pattern = { "*" },
	callback = function()
		local save_cursor = vim.fn.getpos(".")
		pcall(function() vim.cmd [[%s/\s\+$//e]] end)
		vim.fn.setpos(".", save_cursor)
	end,
})

-- TIP: Disable arrow keys in normal mode
-- vim.keymap.set('n', '<left>', '<cmd>echo "Use h to move!!"<CR>')
-- vim.keymap.set('n', '<right>', '<cmd>echo "Use l to move!!"<CR>')
-- vim.keymap.set('n', '<up>', '<cmd>echo "Use k to move!!"<CR>')
-- vim.keymap.set('n', '<down>', '<cmd>echo "Use j to move!!"<CR>')
