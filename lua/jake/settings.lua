local set = vim.opt


-- Use 24-bit (true-color) mode
set.termguicolors = true

-- Set highlight on search
set.hlsearch = true
set.incsearch = true

-- Preview substitutions live, as you type!
vim.opt.inccommand = 'split'

-- Make line numbers default
vim.wo.number = true

-- Enable mouse mode
set.mouse = 'a'

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
--vim.o.clipboard = 'unnamedplus'

-- Indentation
set.breakindent = true
set.tabstop = 4
set.softtabstop = 4
set.shiftwidth = 4
set.expandtab = false

-- Disable line wrap
set.wrap = false

-- Don't save undo history
set.undofile = false

set.swapfile = true

-- Case-insensitive searching UNLESS \C or capital in search
set.ignorecase = true
set.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- Decrease update time
set.updatetime = 250
set.timeoutlen = 300

-- Set completeopt to have a better completion experience
set.completeopt = 'menuone,noselect'


-- Don't let the cursor too close to the bottom or top
set.scrolloff = 8

set.colorcolumn = "80"



vim.g.have_nerd_font = true

-- Don't show the mode, since it's already in the status line
vim.opt.showmode = false

-- Sets how neovim will display certain whitespace characters in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
vim.opt.list = true
vim.opt.listchars = { tab = '» ', trail = '·', nbsp = '␣' }


-- Show which line your cursor is on
vim.opt.cursorline = true

-- Configure diagnostics
vim.diagnostic.config({
	-- https://neovim.io/doc/user/diagnostic.html#vim.diagnostic.Opts
	virtual_text = true,
	signs = {
		text = { ERROR = " ", WARN = " ", HINT = "󰌵", INFO = "" },
	},
	update_in_insert = false,
	underline = true,
	severity_sort = true,
	float = {
		-- https://neovim.io/doc/user/diagnostic.html#vim.diagnostic.Opts.Float
		focusable = false,
		style = "minimal",
		border = "rounded",
		source = true,
		header = "",
		prefix = "",
	},
})
