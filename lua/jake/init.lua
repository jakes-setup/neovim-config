
function is_debug_mode()
	return os.getenv("JAKE_DEBUG_NEOVIM") == "true"
end

-- Overwrite PATH
path = os.getenv("PATH") or "/usr/local/bin:/usr/bin:/bin"
if is_debug_mode() then
	-- don't allow external PATHs during debugging
	path = "/usr/local/bin:/usr/bin:/bin"
end
path = vim.fn.stdpath("config") .. "/deps/bin" .. ":" .. path -- use custom dependencies
vim.env["PATH"] = path -- overwrite what os.getenv("PATH") returns


-- Overwrite JAVA_HOME to fix jdtls
vim.env["JAVA_HOME"] = "/usr/lib/jvm/java-21-openjdk-amd64"

require('jake.settings')
require('jake.remaps')
require('jake.lazy') -- load plugins

