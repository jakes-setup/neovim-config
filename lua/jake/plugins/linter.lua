return {
	{
		-- Linter
		-- https://github.com/mfussenegger/nvim-lint
		'mfussenegger/nvim-lint',
		dependencies = {
			'WhoIsSethDaniel/mason-tool-installer.nvim'
		},
		event = { 'BufReadPre', 'BufNewFile' },
		config = function()
			local lint = require('lint')
			lint.linters_by_ft = {
				markdown = { 'vale', },
				mustache = { 'djlint', },
				html = { 'djlint', 'htmlhint', },
				--python = { 'flake8', 'pylint', },
				--lua = { 'luacheck', }
			}

			if vim.fn.executable('luacheck') == 1 then
				-- enable luacheck only if installed
				lint.linters_by_ft['lua'] = { 'luacheck', }
			end

			if vim.fn.executable('pylint') == 1 then
				-- if pylint is installed, especially in project local venv, then use it for linting
				lint.linters_by_ft['python'] = { 'flake8', 'pylint', }
			else
				lint.linters_by_ft['python'] = { 'flake8', }
			end

			local flake8 = lint.linters.flake8
			-- https://flake8.pycqa.org/en/3.1.1/user/ignoring-errors.html
			-- https://github.com/mfussenegger/nvim-lint/blob/master/lua/lint/linters/flake8.lua
			--print(vim.inspect(flake8.args))
			flake8.args = {
				'--format=%(path)s:%(row)d:%(col)d:%(code)s:%(text)s',
				'--no-show-source',
				--'--ignore=E1,E23,W503',
				'--ignore=E501',
				'--stdin-display-name', function() return vim.api.nvim_buf_get_name(0) end,
				'-',
			}

			--local pylint = lint.linters.pylint

			local luacheck = lint.linters.luacheck
			-- https://luacheck.readthedocs.io/en/stable/cli.html
			-- https://github.com/mfussenegger/nvim-lint/blob/master/lua/lint/linters/luacheck.lua
			luacheck.args = {
				'--formatter', 'plain',
				'--codes',
				'--ranges',
				'--globals', 'vim',
				'-'
			}

			-- Create autocommand which carries out the actual linting
			-- on the specified events.
			local lint_augroup = vim.api.nvim_create_augroup('lint', { clear = true })
			vim.api.nvim_create_autocmd({
				'BufEnter',
				'BufWritePost',
				'InsertLeave'
			}, {
				group = lint_augroup,
				callback = function()
					lint.try_lint()
				end,
			})
		end
	},
}
