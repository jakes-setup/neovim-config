
local function exec_command(command)
	-- http://www.lua.org/manual/5.1/manual.html#pdf-io.popen
	local handle = io.popen(command .. " 2>/dev/null")
	-- local output = handle:read("*a") -- read entire output
	local output = {}
	for line in handle:lines() do
		table.insert(output, line)
	end
	handle.close()
	if #output == 0 then
		return nil
	else
		return output
	end
end


local git_command = "git "
local function git(args, wd)
	local cmd = ""
	if wd then
		cmd = cmd .. "cd \"" .. wd .. "\"; "
	end
	cmd = cmd .. git_command .. args
	return exec_command(cmd)
end


local function is_git()
	return git("branch") ~= nil
end

local function relative_path(from_file, to_dir, _prefix)
	local from = from_file:gmatch("([^/]+)")
	local to = to_dir:gmatch("([^/]+)")

	local _, from_slashes = from_file:gsub("/","")
	local _, to_slashes = to_dir:gsub("/","")

	local prev_to_dir = ""
	local tmp_prev_to_dir = ""

	local f = nil
	local t = nil
	local count = 0
	repeat
		f = from()
		t = to()
		if t ~= nil then
			prev_to_dir = tmp_prev_to_dir
			tmp_prev_to_dir = tmp_prev_to_dir .. "/" .. t
		end
		count = count + 1
	until f ~= t


	if t == nil then -- case 1: from_file is in same directory or in a subdirectory of to_dir
		if _prefix == nil then
			_prefix = ""
		end
		local res = _prefix
		repeat
			res = res .. f
			f = from()
			if f ~= nil then
				res = res .. "/"
			end
		until f == nil
		return res
	else -- case 2: to_dir is deeper than the most common path of from_file and to_dir
		_prefix = ""
		repeat
			_prefix = _prefix .. "../"
			t = to()
		until t == nil
		return relative_path(from_file, prev_to_dir, _prefix)
	end
end

local function betterfind(opts)
	local files = {}
	local cwd = vim.fn.getcwd()
	local toplevel = cwd
	if is_git() then
		-- print("Du bist in einem git Repo.")
		toplevel = git("rev-parse --show-toplevel")[1]

		files = git("ls-files --deduplicate --cached --modified --others --killed --no-empty-directory --full-name", toplevel)
	else
		--print("Du bist NICHT in einem git Repo.")
		files = exec_command("find . -type f")
	end

	local result = {}
	for _, file in pairs(files) do
		if file:sub(-#"/") ~= "/" then

			local relpath = relative_path(toplevel .. "/" .. file, cwd)
			table.insert(result, relpath)
		end
	end

	table.sort(result)

	return result
end

return {
	{
		'nvim-telescope/telescope.nvim',
		branch = '0.1.x',
		dependencies = {
			'nvim-lua/plenary.nvim',
			-- Fuzzy Finder Algorithm which requires local dependencies to be built.
			-- Only load if `make` is available. Make sure you have the system
			-- requirements installed.
			{
				'nvim-telescope/telescope-fzf-native.nvim',
				-- NOTE: If you are having trouble with this installation,
				--			 refer to the README for telescope-fzf-native for more instructions.
				build = 'make',
				cond = function()
					return vim.fn.executable 'make' == 1
				end,
			},
			{ 'nvim-telescope/telescope-ui-select.nvim' },

			-- Useful for getting pretty icons, but requires a Nerd Font.
			{ 'nvim-tree/nvim-web-devicons', enabled = vim.g.have_nerd_font },
		},
		config = function()
			require('telescope').setup {
				extensions = {
					['ui-select'] = {
						require('telescope.themes').get_dropdown(),
					},
				},
			}
			-- Enable Telescope extensions if they are installed
			pcall(require('telescope').load_extension, 'fzf')
			pcall(require('telescope').load_extension, 'ui-select')

			local builtin = require('telescope.builtin')
			local pickers = require("telescope.pickers")
			local finders = require("telescope.finders")
			local make_entry = require("telescope.make_entry")
			local conf = require("telescope.config").values

			vim.keymap.set('n', '<leader>sh', builtin.help_tags, { desc = '[S]earch [H]elp' })
			vim.keymap.set('n', '<leader>sk', builtin.keymaps, { desc = '[S]earch [K]eymaps' })
			vim.keymap.set('n', '<leader>sf', builtin.find_files, { desc = '[S]earch [F]iles' })
			vim.keymap.set('n', '<leader>ss', builtin.builtin, { desc = '[S]earch [S]elect Telescope' })
			vim.keymap.set('n', '<leader>sw', builtin.grep_string, { desc = '[S]earch current [W]ord' })
			vim.keymap.set('n', '<leader>sg', builtin.live_grep, { desc = '[S]earch by [G]rep' })
			vim.keymap.set('n', '<leader>sd', builtin.diagnostics, { desc = '[S]earch [D]iagnostics' })
			vim.keymap.set('n', '<leader>sr', builtin.resume, { desc = '[S]earch [R]esume' })
			vim.keymap.set('n', '<leader>s.', builtin.oldfiles, { desc = '[S]earch Recent Files ("." for repeat)' })
			vim.keymap.set('n', '<leader><leader>', builtin.buffers, { desc = '[ ] Find existing buffrs' })

			vim.keymap.set('n', '<leader>pf', builtin.find_files, {})
			vim.keymap.set('n', '<C-p>', function()
				local files = betterfind()
				for _, f in pairs(files) do
					print(f)
				end
				-- builtin.find_files({find_command=files})
				-- builtin.find_files({find_command={"./telescope.lua"}})
				--builtin.git_files()
				local toplevel = git("rev-parse --show-toplevel")[1]
				builtin.git_files({cwd=toplevel})
				opts = {}
				opts.sorting_strategy = "ascending"
				opts.entry_maker = opts.entry_maker or make_entry.gen_from_file(opts)
			
				--pickers.new(opts, {
				--	prompt_title = "Find Files",
				--	--finder = finders.new_oneshot_job(find_command, opts),
				--	finder = finders.new_table(files),
				--	previewer = conf.file_previewer(opts),
				--	sorter = conf.file_sorter(opts),
				--}):find()
			end, {})
			-- vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
			--vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
			--vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
			vim.keymap.set('n', '<leader>ps', function()
				builtin.grep_string({ search = vim.fn.input("Grep > ")});
			end)
		end
	},

}

