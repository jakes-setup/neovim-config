--	This function gets run when an LSP connects to a particular buffer.
local on_attach = function(event)
	local bufnr = event.buf

	local client = vim.lsp.get_client_by_id(event.data.client_id)
	-- Treesitter highlighting is better than jdtls highlighting in Java.
	-- https://vi.stackexchange.com/questions/43428/how-to-disable-lsp-server-syntax-highlighting
	if client.name == "jdtls" then
		client.server_capabilities.semanticTokensProvider = nil
	end

	local nmap = function(keys, func, desc)
		if desc then
			desc = 'LSP: ' .. desc
		end

		vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
	end

	nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
	nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')
	nmap('<leader>q', function()
		vim.lsp.buf.code_action({
			filter = function(a) return a.isPreferred end,
			apply = true
		})
	end, 'Apply preferred code action ([q]uickfix)')

	local telescope = require('telescope.builtin')

	nmap('gd', telescope.lsp_definitions, '[G]oto [D]efinition')
	nmap('gr', telescope.lsp_references, '[G]oto [R]eferences')
	nmap('gI', telescope.lsp_implementations, '[G]oto [I]mplementation')
	nmap('<leader>D', telescope.lsp_type_definitions, 'Type [D]efinition')
	nmap('<leader>ds', telescope.lsp_document_symbols, '[D]ocument [S]ymbols')
	nmap('<leader>ws', telescope.lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

	-- See `:help K` for why this keymap
	nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
	nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

	-- Lesser used LSP functionality
	nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
	nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
	nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
	nmap('<leader>wl', function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, '[W]orkspace [L]ist Folders')

	-- Create a command `:Format` local to the LSP buffer
	vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
		vim.lsp.buf.format()
	end, { desc = 'Format current buffer with LSP' })

	-- force using lsp omnifunc for completion
	local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
	buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
end

return {
	{
		-- LSP Configuration & Plugins
		'neovim/nvim-lspconfig',
		dependencies = {
			-- Automatically install LSPs to stdpath for neovim
			'williamboman/mason.nvim',
			'williamboman/mason-lspconfig.nvim',

			'WhoIsSethDaniel/mason-tool-installer.nvim',

			'folke/which-key.nvim',

			{ 'j-hui/fidget.nvim', opts = {} },

			'hrsh7th/nvim-cmp',
			'hrsh7th/cmp-nvim-lsp',

			-- Additional lua configuration, makes nvim stuff amazing!
			-- It's important that neodev is setup before LSP stuff
			-- https://github.com/folke/neodev.nvim
			{ 'folke/neodev.nvim', opts = {} },
		},
		config = function()
			vim.api.nvim_create_autocmd("LspAttach", {
				callback = on_attach,
			});

			require('which-key').setup()

			-- Document existing key chains
			require('which-key').add {
				{ '<leader>c', group = '[C]ode' },
				{ '<leader>d', group = '[D]ocument' },
				{ '<leader>r', group = '[R]ename' },
				{ '<leader>s', group = '[S]earch' },
				{ '<leader>w', group = '[W]orkspace' },
				{ '<leader>t', group = '[T]oggle' },
				{ '<leader>h', group = 'Git [H]unk', mode = { 'n', 'v' } },
			}


			-- LSP servers and clients are able to communicate to each other what features they support.
			--  By default, Neovim doesn't support everything that is in the LSP specification.
			--  When you add nvim-cmp, luasnip, etc. Neovim now has *more* capabilities.
			--  So, we create new capabilities with nvim cmp, and then broadcast that to the servers.
			local capabilities = vim.lsp.protocol.make_client_capabilities()

			-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
			capabilities = vim.tbl_deep_extend('force', capabilities,
				require('cmp_nvim_lsp').default_capabilities())


			local servers = require('jake.packages.lsp')
			require('mason-lspconfig').setup({
				handlers = {
					function(server_name)
						if server_name == 'jdtls' then
							-- jdtls will be handled by nvim-jdtls. Mason is only used to install the language server itself
							return
						end
						local server = servers[server_name] or {}
						-- This handles overriding only values explicitly passed
						-- by the server configuration above. Useful when disabling
						-- certain features of an LSP (for example, turning off formatting for tsserver)
						server.capabilities = vim.tbl_deep_extend('force', {}, capabilities,
							server.capabilities or {})

						if (server["settings"] or {})["cmd"] ~= nil then
							server["cmd"] = server["settings"]["cmd"]
						end
						require('lspconfig')[server["overwrite_server_name"] or server_name].setup(server)
					end,
				},
			})
		end
	},
	{
		'williamboman/mason-lspconfig.nvim',
		dependencies = {
			'williamboman/mason.nvim',
			'WhoIsSethDaniel/mason-tool-installer.nvim',
		},
	},

	-- Useful status updates for LSP
	-- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
	{ 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

	{
		-- Java Language Server (jdtls)
		-- https://github.com/mfussenegger/nvim-jdtls
		'mfussenegger/nvim-jdtls',

		dependencies = {
			'neovim/nvim-lspconfig',
		},

		--ft = { 'java' }, -- only enable this plugin in java
		config = function()
			--vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
			--	pattern = {
			--		"*.java",
			--		"*.class",
			--		--"jdt://*",
			--	},
			vim.api.nvim_create_autocmd("FileType", {
				pattern = "java",
				callback = function()
					local jdtlspath = vim.fn.stdpath("data") .. "/mason/packages/jdtls"
					local root_dir = require('jdtls.setup').find_root({ '.git', 'mvnw', 'gradlew' })
					require('jdtls').start_or_attach({
						--cmd = {
						--	'java',
						--	'-Declipse.application=org.eclipse.jdt.ls.core.id1',
						--	'-Dosgi.bundles.defaultStartLevel=4',
						--	'-Declipse.product=org.eclipse.jdt.ls.core.product',
						--	'-Dlog.level=ALL',
						--	'-Xmx1G',
						--	'--add-modules=ALL-SYSTEM',
						--	'--add-opens', 'java.base/java.util=ALL-UNNAMED',
						--	'--add-opens', 'java.base/java.lang=ALL-UNNAMED',
						--	'-jar', jdtlspath .. '/plugins/org.eclipse.equinox.launcher_1.6.700.v20231214-2017.jar', -- WARNING: This will change/break over time! TODO: fix this.
						--	'-configuration', jdtlspath .. '/config_linux',
						--	'-data', '/path/to/data', -- TODO
						--},
						cmd = {
							jdtlspath .. "/jdtls",
							'-configuration', vim.fn.stdpath('cache') .. '/jdtls',
							'-data', vim.fn.stdpath('cache') ..
						"/jdtls_projects/" .. vim.fn.fnamemodify(root_dir, ':p:h:t'),
						},

						-- One dedicated LSP server & client will be started per unique root_dir
						root_dir = root_dir,

						-- Language server `initializationOptions`
						-- You need to extend the `bundles` with paths to jar files
						-- if you want to use additional eclipse.jdt.ls plugins.
						--
						-- See https://github.com/mfussenegger/nvim-jdtls#java-debug-installation
						--
						-- If you don't plan on using the debugger or other eclipse.jdt.ls plugins you can remove this
						init_options = {
							--	bundles = {},

							-- Here you can configure eclipse.jdt.ls specific settings
							-- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
							-- for a list of options
							settings = {
								java = {
									autobuild = {
										enabled = false,
									},
									references = {
										includeDecompiledSources = true,
									},
									contentProvider = {
										-- https://github.com/dgileadi/vscode-java-decompiler
										preferred = "fernflower",
									},
									project = {
										--sourcePaths = {
										--	"/home/jake/stuff/app/sag/src/",
										--	"/home/jake/stuff/app/sag/src/**/*.java",
										--	"src/**/*.java",
										--	"lib/**/*.jar",
										--	"libs/**/*.jar",
										--	"library/**/*.jar",
										--},
										referencedLibraries = {
											include = {
												"lib/**/*.jar",
												"libs/**/*.jar",
												"library/**/*.jar",
												--"/home/username/lib/foo.jar",
											},
											sources = {
												['lib/Simple-APP-Graphics-v2.1.0.jar'] =
												'lib/Simple-APP-Graphics-v2.1.0-source.jaar'
											},

										}
									},
									imports = {
										gradle = {
											wrapper = {
												checksums = {
													{
														sha256 =
														"979efcaf2695244a3049d8100ee8d310076e60cc9d596b1a6dd07db8eb921fc1",
														allowed = true,
													},
													{
														sha256 =
														"41c8aa7a337a44af18d8cda0d632ebba469aef34f3041827624ef5c1a4e4419d",
														allowed = true,
													},
												}
											},
										}
									}
								}
							},
						},
					})
				end
			});
		end,


	}

}
