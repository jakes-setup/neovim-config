return {
	{
		-- https://github.com/AndrewRadev/dealwithit.vim
		'AndrewRadev/dealwithit.vim',
		cmd = {
			'Dealwithit',
		},
		init = function()
			-- vim.g.dealwithit_guifont = 'Fantasque Sans Mono 8'
			vim.g.dealwithit_sleep = 20
		end,
	},
	{
		-- https://github.com/tamton-aquib/duck.nvim
		'https://gitlab.gwdg.de/jakes-setup/duck.nvim.git',
		-- 'tamton-aquib/duck.nvim',
		config = function()
			local duck = require("duck")

			local schedule_wrap = function(fn)
				return vim.schedule_wrap(function()
					if not pcall(fn) then
						-- cook all ducks on error
						duck.cook_all()
					end
				end)
			end

			duck.setup({
				schedule_wrap = schedule_wrap
			})

			local fast = 15
			local normal = 10
			local slow = 3
			-- https://emojicopy.com/
			local ducks = {
				{ duck = "🦆", speed = normal, },
				{ duck = "🪿", speed = normal, },
				{ duck = "🐤", speed = normal, },
				{ duck = "🐦", speed = normal, },
				{ duck = "🐥", speed = normal, },
				{ duck = "🐇", speed = fast, },
				{ duck = "🐁", speed = fast, },
				{ duck = "🐀", speed = fast, },
				{ duck = "🕊️", speed = fast, },
				{ duck = "🦜", speed = normal, },
				{ duck = "🦢", speed = slow, },
				{ duck = "🦚", speed = normal, },
				{ duck = "🦤", speed = normal, },
				{ duck = "🦃", speed = normal, },
				{ duck = "🐓", speed = normal, },
				{ duck = "🦭", speed = slow, },
			}
			local bugs = {
				{ duck = "🪱", speed = slow, },
				{ duck = "🐝", speed = normal, },
				{ duck = "🐛", speed = slow, },
				{ duck = "🐌", speed = slow, },
				{ duck = "🐞", speed = slow, },
				{ duck = "🐜", speed = slow, },
				{ duck = "🦋", speed = fast, },
				{ duck = "🪰", speed = fast, },
				{ duck = "🪲", speed = slow, },
				{ duck = "🪳", speed = slow, },
				{ duck = "🦟", speed = slow, },
				{ duck = "🦗", speed = slow, },
				{ duck = "🕷️", speed = slow, },
				{ duck = "🦂", speed = slow, },
			}

			local function get_random_duck(bugchance)
				local pool = ducks
				if math.random() < bugchance then
					pool = bugs
				end
				local index = math.random(#pool)
				local selected = pool[index]
				return selected
			end

			local function hatch_random(bugchance)
				local selected = get_random_duck(bugchance)

				-- hatch the duck
				duck.hatch(selected["duck"], selected["speed"])
			end

			vim.api.nvim_create_user_command("DuckHatchRandom", function(opts)
				if #opts.fargs == 1 then
					hatch_random(tonumber(opts.fargs[1]))
				else
					hatch_random(0.5)
				end
			end, {
				force = false,
				nargs = "?", -- 0 or 1 arguments
			})
			vim.api.nvim_create_user_command("DuckCookRandom", function()
				duck.cook_random(true)
			end, {
				force = false,
			})
			vim.api.nvim_create_user_command("DuckCookAll", function()
				duck.cook_all()
			end, {
				force = false,
			})

			if #vim.api.nvim_list_uis() > 0 then
				local timer = vim.uv.new_timer()
				local interval = 1000 * 10 -- 10 seconds
				local num_bugs = 0
				timer:start(interval / 3, interval, schedule_wrap(function()
					-- We use the number of diagnostics as a rough indicator of number of bugs in the code. :)
					local num_diagnostic = #vim.diagnostic.get(0, {
						severity = {
							-- vim.diagnostic.severity.HINT,
							vim.diagnostic.severity.INFO,
							vim.diagnostic.severity.WARN,
							vim.diagnostic.severity.ERROR,
						}
					})

					num_diagnostic = num_diagnostic / 3  -- reduce number of bugs

					-- try to have bug population match the number of diagnostics
					if num_diagnostic > num_bugs then
						hatch_random(1)
						num_bugs = num_bugs + 1
					elseif num_diagnostic < num_bugs then
						duck.cook_random(false)
						num_bugs = num_bugs - 1
					end

					-- 5% chance of an additional duck action
					if math.random() * 100 <= 5 then
						-- A duck action can either be hatching or cooking a random duck.
						if math.random() * 100 <= 50 then
							-- The more diagnostics exist, the more bugs should appear.
							local bugchance = (10 * (num_diagnostic * 10)) / 100
							hatch_random(bugchance)
						else
							duck.cook_random(false)
						end
					end
				end))
			end
		end
	},
	{
		-- https://github.com/Eandrju/cellular-automaton.nvim
		'Eandrju/cellular-automaton.nvim',
		cmd = {
			'CellularAutomaton',
			'CellularAutomatonRandom',
		},
		config = function()
			local ca = require("cellular-automaton")

			-- [[ Register the CellularAutomatonRandom command ]]
			vim.api.nvim_create_user_command("CellularAutomatonRandom", function()
				local num_animations = 0
				for _, _ in pairs(ca.animations) do
					num_animations = num_animations + 1
				end
				local selected = math.random(num_animations)
				local index = 0
				for animation_name, _ in pairs(ca.animations) do
					index = index + 1
					if index == selected then
						ca.start_animation(animation_name)
						return
					end
				end
				vim.notify("Failed to even find a random animation to play. This should never happen.",
					vim.log.levels.ERROR)
			end, {
				force = false,
			})

			-- [[ Add custom automatons ]]
			ca.register_animation({
				name = 'slide',
				fps = 50,
				update = function(grid)
					-- Source: https://github.com/Eandrju/cellular-automaton.nvim?tab=readme-ov-file#implementing-your-own-cellular-automaton-logic
					for i = 1, #grid do
						local prev = grid[i][#(grid[i])]
						for j = 1, #(grid[i]) do
							grid[i][j], prev = prev, grid[i][j]
						end
					end
					return true
				end
			})
		end
	},
}
