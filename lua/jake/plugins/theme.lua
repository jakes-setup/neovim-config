return {
	{
		-- Theme
		'morhetz/gruvbox',
		config = function()
			vim.g.gruvbox_italic = 1
			vim.cmd.colorscheme("gruvbox")

			-- set background to transparent
			-- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
			vim.api.nvim_set_hl(0, "NormalFloat", { bg = "#111111" })
		end
	}
}

