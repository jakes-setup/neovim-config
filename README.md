# Neovim Config

Diese Config kompiliert Neovim selber.

## Abhängigkeiten

- Git >=2.19.0
- Clang oder GCC >=4.9+
- CMake >=3.10+ (built with TLS/SSL support)
- npm / NodeJS
- Das Terminal sollte ein [NerdFont](https://www.nerdfonts.com) verwenden.

```bash
sudo apt-get install ninja-build gettext cmake unzip curl git tar build-essential libstdc++6
```
